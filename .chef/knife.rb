# See http://docs.chef.io/config_rb_knife.html for more information on knife configuration options

current_dir = __dir__
log_level                :info
log_location             STDOUT
node_name                "ankitdimania"
client_key               "#{current_dir}/ankitdimania.pem"
chef_server_url          "https://api.chef.io/organizations/ankitd-test"
cookbook_path            ["#{current_dir}/../cookbooks"]
