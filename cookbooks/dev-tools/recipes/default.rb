#
# Cookbook Name:: dev-tools
# Recipe:: default
#
# Copyright 2016, GrootStock
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'apt::default'
include_recipe 'chef-client::default'

include_recipe 'dev-tools::htop'
include_recipe 'dev-tools::vim'
