name             'dev-tools'
maintainer       'Ankit Dimania'
maintainer_email 'ankitdimania@grootstock.com'
license          'All rights reserved'
description      'Installs/Configures dev-tools'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.3'

depends 'apt', '~> 2.9'
depends 'chef-client', '~> 4.6'
