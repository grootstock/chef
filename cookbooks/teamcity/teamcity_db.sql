create database teamcity character set UTF8 collate utf8_bin;
USE teamcity
create user 'teamcityuser'@'localhost' identified by '<password>';
grant all privileges on teamcity.* to 'teamcityuser'@'localhost';
