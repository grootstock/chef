#
# Cookbook Name:: teamcity
# Recipe:: default
#
# Copyright (c) 2016 GrootStock, All Rights Reserved.

teamcity_image = 'jetbrains/teamcity-server'
teamcity_image_tag = '2017.1.2'

docker_image teamcity_image do
  tag teamcity_image_tag
  action :pull
end

teamcity_base_dir = '/media/data/teamcity'
teamcity_data_dir = "#{teamcity_base_dir}/data"
teamcity_log_dir = "#{teamcity_base_dir}/logs"

docker_container 'ci_server' do
  repo teamcity_image
  tag teamcity_image_tag
  port '8111:8111'
  host_name 'teamcity'
  restart_policy 'on-failure'
  restart_maximum_retry_count 3
  kill_after 120
  volumes %W(#{teamcity_data_dir}:/data/teamcity_server/datadir #{teamcity_log_dir}:/opt/teamcity/logs)
end

# Create the teamcity directory.
teamcity_jdbc_dir = "#{teamcity_data_dir}/lib/jdbc"

directory teamcity_jdbc_dir do
  recursive true
end

# Write the mysql connector.
remote_file "#{teamcity_jdbc_dir}/mysql-connector-java-5.1.43.jar" do
  source 'http://repo1.maven.org/maven2/mysql/mysql-connector-java/5.1.43/mysql-connector-java-5.1.43.jar'
  owner 'root'
  group 'root'
  mode '0755'
end
