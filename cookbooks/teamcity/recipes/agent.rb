#
# Cookbook Name:: teamcity
# Recipe:: agent
#
# Copyright (c) 2016 GrootStock, All Rights Reserved.

teamcity_agent_image = 'jetbrains/teamcity-agent'
teamcity_agent_tag   = '2017.1.2'

docker_image teamcity_agent_image do
  tag teamcity_agent_tag
  action :pull
end

teamcity_agent_base_dir = '/media/data/teamcity_agent'
teamcity_agent_conf_dir = "#{teamcity_agent_base_dir}/conf"

docker_container 'ci_agent' do
  repo teamcity_agent_image
  tag teamcity_agent_tag
  # privileged true
  publish_all_ports
  env %w(SERVER_URL="http://ci.grootstock.com:8111" DOCKER_IN_DOCKER="start")
  volumes %W(#{teamcity_agent_conf_dir}:/data/teamcity_agent/conf)
  restart_policy 'on-failure'
  restart_maximum_retry_count 3
  kill_after 120
end
