name 'teamcity'
maintainer 'Ankit Dimania'
maintainer_email 'ankitdimania@grootstock.com'
license 'All rights reserved'
description 'Installs/Configures teamcity'
long_description 'Installs/Configures teamcity'
version '0.1.18'

depends 'docker', '~> 2.0'
