name 'docker-dind'
maintainer 'Ankit Dimania'
maintainer_email 'ankitdimania@grootstock.com'
license 'All rights reserved'
description 'Installs/Configures docker-dind'
long_description 'Installs/Configures docker-dind'
version '0.1.1'

depends 'docker', '~> 2.0'
