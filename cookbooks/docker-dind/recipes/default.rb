#
# Cookbook Name:: docker-dind
# Recipe:: default
#
# Copyright (c) 2016 GrootStock, All Rights Reserved.

docker_image = 'docker'
docker_image_tag = '1.11-dind'

docker_image docker_image do
  tag docker_image_tag
  action :pull
end

docker_container 'Docker_Server' do
  repo docker_image
  tag docker_image_tag
  privileged true
  restart_policy 'on-failure'
  restart_maximum_retry_count 3
end
