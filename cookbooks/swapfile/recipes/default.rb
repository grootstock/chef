#
# Cookbook Name:: swapfile
# Recipe:: default
#
# Copyright (c) 2016 GrootStock, All Rights Reserved.

swapfile_path = node['swapfile']['path']
swapfile_size = node['swapfile']['size']

swap_file swapfile_path do
  size swapfile_size # MBs
end
