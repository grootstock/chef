name 'swapfile'
maintainer 'Ankit Dimania'
maintainer_email 'ankitdimania@grootstock.com'
license 'All rights reserved'
description 'Installs/Configures swapfile'
long_description 'Installs/Configures swapfile'
version '0.1.0'

depends 'swap', '~> 0.3.8'
