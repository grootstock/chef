#
# Cookbook Name:: zeroconf
# Recipe:: default
#
# Copyright 2016, GrootStock
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'apt::default'
include_recipe 'chef-client::default'

include_recipe 'zeroconf::avahi'
include_recipe 'zeroconf::libnss_mdns'
