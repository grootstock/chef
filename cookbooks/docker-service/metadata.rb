name 'docker-service'
maintainer 'Ankit Dimania'
maintainer_email 'ankitdimania@grootstock.com'
license 'All rights reserved'
description 'Installs/Configures docker-service'
long_description 'Installs/Configures docker-service'
version '0.1.2'

depends 'docker', '~> 2.0'
