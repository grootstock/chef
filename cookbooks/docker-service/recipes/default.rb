#
# Cookbook Name:: docker-service
# Recipe:: default
#
# Copyright (c) 2016 GrootStock, All Rights Reserved.

docker_service 'default' do
  action [:create, :start]
end
