#
# Cookbook Name:: redis
# Recipe:: default
#
# Copyright (c) 2016 GrootStock, All Rights Reserved.

redis_image = 'redis'
redis_image_tag = '3.2'

docker_image redis_image do
  tag redis_image_tag
  action :pull
end

redis_base_dir = '/media/data/redis'
redis_data_dir = "#{redis_base_dir}/data"

docker_container 'redis' do
  repo redis_image
  tag redis_image_tag
  port '6379:6379'
  host_name 'redis'
  restart_policy 'on-failure'
  restart_maximum_retry_count 3
  kill_after 120
  volumes %W(#{redis_data_dir}:/data)
end
