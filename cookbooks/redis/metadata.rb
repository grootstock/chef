name 'redis'
maintainer 'Ankit Dimania'
maintainer_email 'ankitdimania@grootstock.com'
license 'All rights reserved'
description 'Installs/Configures redis'
long_description 'Installs/Configures redis'
version '0.1.1'

depends 'docker', '~> 2.0'
