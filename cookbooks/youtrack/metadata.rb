name 'youtrack'
maintainer 'Ankit Dimania'
maintainer_email 'ankitdimania@grootstock.com'
license 'All rights reserved'
description 'Installs/Configures youtrack'
long_description 'Installs/Configures youtrack'
version '0.1.5'

depends 'docker', '~> 2.0'
