#
# Cookbook Name:: youtrack
# Recipe:: default
#
# Copyright (c) 2016 GrootStock, All Rights Reserved.

youtrack_image = 'uniplug/youtrack'

docker_image youtrack_image do
  # tag 'latest'
  action :pull
end


docker_container 'Issue_Tracker' do
  repo youtrack_image
  # tag 'latest'
  port '8080:80'
  host_name 'youtrack'
  restart_policy 'on-failure'
  restart_maximum_retry_count 3
  volumes %w(/media/data/youtrack/data/:/opt/youtrack/data/ /media/data/youtrack/backup/:/opt/youtrack/backup/)
end
