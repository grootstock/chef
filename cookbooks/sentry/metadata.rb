name 'sentry'
maintainer 'Ankit Dimania'
maintainer_email 'ankitdimania@grootstock.com'
license 'All rights reserved'
description 'Installs/Configures sentry'
long_description 'Installs/Configures sentry'
version '0.1.6'

depends 'docker-service', '~> 0.1'
