#
# Cookbook Name:: sentry
# Recipe:: default
#
# Copyright (c) 2016 GrootStock, All Rights Reserved.

## Install redis
# don't worry about data, we don't care to keep it
# long term anyway

include_recipe 'docker-service::default'

redis_image = 'redis'

docker_image redis_image do
  action :pull
end

docker_container 'sentry-redis' do
  repo redis_image
  restart_policy 'on-failure'
  restart_maximum_retry_count 3
  volumes ['/media/data/sentry-redis/data/:/data']
end

postgres_image = 'postgres'

docker_image postgres_image do
  action :pull
end

sentry_postgres_pass = data_bag_item('credentials', 'sentry_postgres')['pass']

docker_container 'sentry-postgres' do
  repo postgres_image
  env ["POSTGRES_PASSWORD=#{sentry_postgres_pass}", 'POSTGRES_USER=sentry']
  restart_policy 'on-failure'
  restart_maximum_retry_count 3
  volumes ['/media/data/sentry-postgresql/data/:/var/lib/postgresql/data']
end

sentry_image = 'sentry'

docker_image sentry_image do
  action :pull
end

sentry_secret_key = data_bag_item('credentials', 'sentry_secret_key')['key']

## For the first time
# setup database
# docker run -it --rm -e SENTRY_SECRET_KEY=<key> --link sentry-postgres:postgres --link sentry-redis:redis sentry upgrade

sentry_port = node['sentry']['port']

docker_container 'sentry' do
  repo sentry_image
  port "#{sentry_port}:9000"
  links %w(sentry-postgres:postgres sentry-redis:redis)
  env ["SENTRY_SECRET_KEY='#{sentry_secret_key}'"]
  restart_policy 'on-failure'
  restart_maximum_retry_count 3
  volumes ['/media/data/sentry/files:/var/lib/sentry/files']
end

## One time upgrade
# docker run -it --rm -e SENTRY_SECRET_KEY='<secret-key>' --link sentry-postgres:postgres --link sentry-redis:redis sentry upgrade

## Install google auth
# TODO: this could be moved to sentry image
# docker exec sentry pip install https://github.com/getsentry/sentry-auth-google/archive/master.zip --upgrade

docker_container 'sentry-cron' do
  repo sentry_image
  links %w(sentry-postgres:postgres sentry-redis:redis)
  env ["SENTRY_SECRET_KEY='#{sentry_secret_key}'"]
  restart_policy 'on-failure'
  restart_maximum_retry_count 3
  command %w(run cron)
end

docker_container 'sentry-worker-1' do
  repo sentry_image
  links %w(sentry-postgres:postgres sentry-redis:redis)
  env ["SENTRY_SECRET_KEY='#{sentry_secret_key}'"]
  restart_policy 'on-failure'
  restart_maximum_retry_count 3
  command %w(run worker)
end
