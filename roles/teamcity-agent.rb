name 'teamcity-agent'
description 'Setup teamcity agent'
run_list 'recipe[docker-service]', 'recipe[teamcity::agent]'