#/bin/sh!

if [ -z $1 -o -z $2 ];then
  echo "Usage: `basename $0` <node dns/ip> <node-name>"
fi

user=ubuntu

if [ -n "$3" ];then
  user=ec2-user
fi

echo $user

knife bootstrap $1 -N $2 --ssh-user $user -i ../grootstock-devo.pem --sudo --verbose
